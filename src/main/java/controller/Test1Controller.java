package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import utils.GenerateNum;

import java.util.Arrays;
import java.util.List;

/**
 * @author :chenping
 * @date :Created in 2021/12/16 0:13
 */
public class Test1Controller {

    @FXML
    private Label label11;

    @FXML
    private Label label12;

    @FXML
    private Label label13;

    @FXML
    private Label label14;

    @FXML
    private Label label15;

    @FXML
    private Label label16;

    @FXML
    private Label blue1;

    @FXML
    private Label label21;

    @FXML
    private Label label22;

    @FXML
    private Label label23;

    @FXML
    private Label label24;

    @FXML
    private Label label25;

    @FXML
    private Label label26;

    @FXML
    private Label blue2;

    @FXML
    private Button generate;



    @FXML
    void generateClick(ActionEvent event) {
        List<Label> first = Arrays.asList(label11, label12, label13, label14, label15, label16);
        List<Label> second = Arrays.asList(label21, label22, label23, label24, label25, label26);
        GenerateNum.generateRedNum(first);
        GenerateNum.generateBlueNum(blue1);
        GenerateNum.generateRedNum(second);
        GenerateNum.generateBlueNum(blue2);
    }

}
