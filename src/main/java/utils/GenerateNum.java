package utils;

import javafx.scene.control.Label;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 号码生成工具
 * @author :chenping
 * @date :Created in 2021/12/19 16:51
 */
public class GenerateNum {
    public final static String [] NUMBER = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
                                    "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
                        "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33"};



    public static void generateRedNum(List<Label> ballLabel){
        List<String> redBalls = new ArrayList<>();
        for(int i = 0; i < ballLabel.size(); i++){
            pushRedBall(redBalls);
        }
        redBalls.sort((e1, e2) -> e1.compareTo(e2));

        for(int j = 0; j < redBalls.size(); j++){
            String num = redBalls.get(j);
            Label label = ballLabel.get(j);
            label.setText(num);
        }
    }

    public static void generateBlueNum(Label label){
        String num = generateBall(16);
        label.setText(num);
    }

    public static String generateBall(int size){
        Random random = new Random();
        int num = random.nextInt(size);
        if(num > 32){
            num = 32;
        }
        String ball = NUMBER[num];
        return ball;
    }

    public static void pushRedBall(List<String>balls){
        String num = generateBall(33);
        if(balls.contains(num)){
            pushRedBall(balls);
        }else{
            balls.add(num);
        }
    }

}
