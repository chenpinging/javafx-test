import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * @author :chenping
 * @date :Created in 2021/12/15 21:53
 */
public class App extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Pane root = FXMLLoader.load(getClass().getResource("fxml/test1.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("福利彩票预测神奇");
        stage.show();
    }

    /**
     * 初始化调用方法
     * @author  : chenping
     * @date    : 2021/12/15 22:08
     * @param
     */
    @Override
    public void init() throws Exception{
        System.out.println("初始化成功");
    }

    /**
     * 程序停止调用函数
     * @author  : chenping
     * @date    : 2021/12/15 22:11
     * @param
     */
    @Override
    public void stop() throws Exception{
        System.out.println("程序退出");
    }



    public static void main(String []args){
        Application.launch(args);
    }
}
